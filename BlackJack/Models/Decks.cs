using System.Collections.Generic;

namespace BlackJack.Models
{
    public class Deck
    {
        public List<Card> Cards { get; set; }
    }
}