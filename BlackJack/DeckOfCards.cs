using System.Collections.Generic;
using BlackJack.Models;

namespace BlackJack
{
    public static class DeckOfCards
    {
        public static Deck Create()
        {
            var deck = new Deck {Cards = new List<Card>()};
            var suits = new[] {"Clubs", "Spades", "Hearts", "Diamonds"};
            var values = new[] {"A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"};
            foreach (var value in values)
            {
                foreach (var suit in suits)
                {
                   deck.Cards.Add(new Card()
                   {
                       suit = suit,
                       value = value,
                       visable = false
                   });         
                } 
            }

            return deck;
        }
    }
}