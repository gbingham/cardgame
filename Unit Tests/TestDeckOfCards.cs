using System;
using System.Collections.Generic;
using System.Linq;
using BlackJack;
using BlackJack.Models;
using FluentAssertions;
using NUnit.Framework;

namespace Unit_Tests
{
    public static class TestDeckOfCards
    {
        [Test]
        public static void Create()
        {
            var x = new Deck();
            x.Should().NotBeNull();
            x.Should().BeOfType<Deck>();
            x.Cards.Should().NotBeNull();
            x.Cards.Should().HaveCount(52);
            x.Cards.Should().BeOfType<List<Card>>();
            x.Cards.FirstOrDefault().Should().NotBeNull();
            x.Cards.FirstOrDefault().Should().BeOfType<Card>();
            x.Cards.FirstOrDefault().suit.Should().NotBeNull();
            x.Cards.FirstOrDefault().value.Should().NotBeNull();
            x.Cards.FirstOrDefault().visable.Should().BeFalse();
            var suits = new[] {"Clubs", "Spades", "Hearts", "Diamonds"};
            var values = new[] {"A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"};
            foreach (var suit in suits)
            {
                foreach (var value in values)
                {
                    var card = x.Cards.Exists(item => item.suit == suit && item.value == value);
                    card.Should().BeTrue();
                }
            }
        }

        public static object DeckOfCards { get; }
    }
}